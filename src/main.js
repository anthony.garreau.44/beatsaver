import Vue from 'vue';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import VueDisqus from 'vue-disqus'
import App from './App.vue';

Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(VueDisqus);
Vue.config.productionTip = false;

import Beat from './components/Beat.vue';
import Detail from './components/Detail.vue';

const routes = [
  { 'name': 'default', 'path': '/', 'component': Beat },
  { 'name': 'detail', 'path': '/detail/:id', 'component': Detail }
]

const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App)
})
.$mount('#app');

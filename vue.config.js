const path = require('path');
const webpack = require('webpack');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
console.log('TOTOTO');
module.exports = {
  configureWebpack: {
    output: {
      publicPath: '/dev/',
    },
    plugins: [
      new webpack.LoaderOptionsPlugin({
        // test: /\.xxx$/, // may apply this only for some modules
        options: {
          outputDir: path.resolve(__dirname, './dist/dev/')
        }
      })
    ]
  },
};
